import streamlit as st

import os
from os import path
from os.path import join
import sys

import pandas as pd

import src.settings.base as stg
from src.infrastructure.dataframecreation import DataFrameBuilder, random_date_data
from src.domain.featureselection import FeatureSelector
from src.application.decision_model import Speed_Dating_Prediction

from sklearn.model_selection import train_test_split


def project_description():
    """Streamlit widget to show the project description on the home page."""
    # Text/Title
    st.title("Prédictions des décisions de la soirée")
    # Image
    st.image("https://scontent-cdt1-1.xx.fbcdn.net/v/t1.18169-9/29573346_201942453733205_1339004774757620300_n.jpg?_nc_cat=103&ccb=1-3&_nc_sid=973b4a&_nc_ohc=KUwutiJ8aq4AX-k4EAF&_nc_ht=scontent-cdt1-1.xx&oh=8b826e9543d2496947229585d149b978&oe=60DB5EF3",
             width=700,
             caption=" ")
    # Header/Subheader
    st.header("Par **Damien Mellot**")
    st.header("Sopra Steria, **Projet Speed Dating**, juin 2021")
    
def get_predictions():
    
    st.header("Obtenir les données et les prédictions de la soirée en cliquant sur le bouton ci-dessous: ")
    st.subheader("")
    
    def Appuyer():
        
        test_size = stg.TEST_SIZE
        seed = stg.SEED
        
        X = FeatureSelector().women_decisions_features
        Y = FeatureSelector().women_decisions_target
        X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=test_size, random_state=seed)
        
        rdd = random_date_data()
        pred = Speed_Dating_Prediction(X, Y, X_train, X_test, Y_train, Y_test).speed_dating_predictions(rdd)
        df_pred = pd.DataFrame(pred, columns=stg.TARGET)
        df_pred.to_csv(join(stg.DATA_DIR, "night_data.csv"), index=False)
    
    if st.button('Appuyer'):
        Appuyer()
        st.image("https://miro.medium.com/max/1534/1*LtjX9ze971QwTHa7GKO3pA.gif",
             width=700,
             caption=" ")
        st.warning('Les décisions de la soirée vous attendent sous format csv dans le dossier data. Merci de récupérer les données avant de procéder à une nouvelle prédiction.')
        st.error('0 = décision négative')
        st.success('1 = décision positive')
    
if __name__ == "__main__":
    project_description()
    get_predictions()