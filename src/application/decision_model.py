import os
from os import path
from os.path import join
import sys

import pandas as pd

import src.settings.base as stg
from src.infrastructure.dataframecreation import DataFrameBuilder
from src.domain.featureselection import FeatureSelector

from sklearn.model_selection import train_test_split, KFold, cross_val_score
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import recall_score

class Speed_Dating_Prediction:
    
    """ Select the appropriate models for women's decisions predictions. 

    Attributes
    ----------
    
    Methods
    -------

    """
    def __init__(self, X, Y, X_train, X_test, Y_train, Y_test):
        
        # Chargement des données, features et target
        self.X = FeatureSelector().women_decisions_features
        self.Y = FeatureSelector().women_decisions_target
        
        # Train - Test split
        self.X_train, self.X_test, self.Y_train, self.Y_test = train_test_split(self.X, self.Y, test_size=stg.TEST_SIZE, random_state=stg.SEED)
    
    def gnb_decision_predictor(self):
        
        gnb = GaussianNB()
        gnb.fit(self.X_train, self.Y_train)
    
        return gnb
    

    def gnb_project_predictions(self):
    
        predictor = self.gnb_decision_predictor()
        predictions = predictor.predict(self.X_test)
        
        return predictions
    
    def gnb_project_recall(self):
    
        rec = recall_score(self.Y_test, self.gnb_project_predictions())
        
        return rec
    
    def single_prediction(self,
                          attractivity=5.0,
                          sincerity=5.0,
                          intelligence=5.0,
                          fun=5.0, 
                          ambition=5.0, 
                          shared_interests=5.0):
        
        predictor = self.gnb_decision_predictor()
        prediction = predictor.predict(pd.DataFrame({"attractivity": [attractivity],
                                                     "sincerity": [sincerity],
                                                     "intelligence": [intelligence],
                                                     "fun": [fun],
                                                     "ambition": [ambition],
                                                     "shared_interests": [shared_interests]}, index = [1]))
        
        return prediction
    
    def speed_dating_predictions(self, test_data):
    
        predictor = self.gnb_decision_predictor()
        predictions = predictor.predict(test_data)
        
        return predictions
    
    
    def random_forest_classifier(self):
            
        rfc = RandomForestClassifier()
        rfc.fit(self.X_train, self.Y_train)
    
        return rfc