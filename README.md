# Speed-dating experiment

![alt text](https://cache.cosmopolitan.fr/data/photo/w2000_ci/57/bon-baiser-de-bruges.jpg)

Quels attributs influencent la sélection par une femme de son partenaire romantique?

Qu’est-ce-qui favorise le coup de foudre dans les premières minutes de la rencontre? 

La question des déterminants de la rencontre amoureuse et de la séduction en général ne cessent de fasciner. Ils sont le sujet éternel de la littérature, du cinéma et de la musique. Bien que l'approche y soit très différente, la question interroge aussi la science, et notamment les sciences sociales.

A l'heure du tout digital et des médias sociaux, la data et son florilège d'informations peut-elle nous révéler des liens cachés, des motivations insoupçonnées, voire des déterminismes puissants dans nos comportements amoureux et nos préférences sexuelles? La question intéresse et mérite d'être explorée!

Le jeu de données que nous utilisons été compilé par les professeurs Ray Fisman et Sheena Iyengar de la Columbia Business School pour leur papier « Gender Differences in Mate Selection : Evidence From a Speed Dating Experiment » .

Les données ont été récupérées de participants à des événements de speed dating expérimentaux entre 2002 et 2004. Lors des événements, les participants avaient un « premier date » de quatre minute avec tous les autres participants du sexe opposé. A la fin de leurs quatre minutes, on demandait à ceux-ci si ils voulaient revoir leur partenaire à nouveau. On leur a aussi demandé de noter celui-ci sur six attributs : Attractivité, Sincérité, Intelligence, Fun, Ambition et Passions communes. 

Le dataset inclut aussi des réponses de questionnaire regroupées à partir des participants à différents moments du processus. Ces champs d’intérêts inclus : aspects démographiques, habitudes romantiques, autoperception autour de caractéristiques clés, croyances sur ce que les autres trouvent valorisé chez un partenaire romantique, informations sur le style de vie. 

Nous nous pencherons dans ce projet sur ce qui motive la décision du côté des femmes.

**La nouvelle algocratie peut-elle révéler le secret du désir féminin?**

### Requirements 
Les outils suivants doivent être installés avec le projet:
* `python >= 3.8`
* `poetry >= 1.1`

### Setup environment
Utilisez les commandes suivantes pour installer le projet.
```
Par SSH
$ git clone git@gitlab.com/damien-mellot/speed-dating.git

Par https
$ git clone https://gitlab.com/damien-mellot/speed-dating.git


Calibrez votre pythonpath
export PYTHONPATH="$PYTHONPATH:/Users/path/to/speed-dating/"

$ poetry install  # Installer l'environnement virtuel avec les packages du fichier pyproject.toml.
$ poetry shell # Activer l'environnement virtuel

``` 

### Run script

Pour lancer un script, vous pouvez suivre les étapes suivantes:

- 1) **Prédiction individuelle personnalisée**: depuis votre bureau, rentrez *cd speed-dating/src/interface/*, puis rentrez *streamlit run single_prediction_interface.py*. Vous verrez une page s'ouvrir dans votre navigateur web, vous permettant de faire une prédiction individuelle de décision romantique personnalisée. Pour la variable **Passions Communes**, gardée à ce stade du projet mais peu à même d'être évaluée en l'absence de rendez-vous réel, entrez la valeur par défaut **6.0**.

- 2) **Paquet de prédictions d'une soirée de dating**: depuis votre bureau, rentrez *cd speed-dating/src/interface/*, puis rentrez *streamlit run full_night_interface.py*. Vous verrez une page s'ouvrir dans votre navigateur web, vous permettant d'obtenir un fichier csv (disponible dans le dossier **data/**) correspondant aux prédictions d'une soirée complète de speed dating pour une même femme. Ce fichier est à retirer du dossier pour pouvoir obtenir le fichier de prédictions suivant. Il s'agit d'un *work in progress*.

- 3) Dans les deux cas : - **0** correspond à une **décision négative** pour un nouveau rendez-vous. 
                         - **1** correspond à une **décision positive** pour un nouveau rendez-vous.